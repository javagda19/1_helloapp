package com.javagda19.helloapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;

@Controller
@RequestMapping(path = "/controller")
public class HelloController {

    // localhost:8080/controller/hello
    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String helloWorld() {
        // zwracana wartość to nazwa template.
        return "helloThymeleaf";
    }

    @RequestMapping(path = "/loop", method = RequestMethod.GET)
    public String helloLoop(Model model) {
        model.addAttribute("listToIterate", new ArrayList<>(Arrays.asList("Gosia", "Tosia", "Ola", "Ala")));

        return "helloLoopThymeleaf";
    }

}
