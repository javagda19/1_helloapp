package com.javagda19.helloapp.controller;

import com.javagda19.helloapp.exception.RequiredFieldEmptyException;
import com.javagda19.helloapp.model.User;
import com.javagda19.helloapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(path = "/add_v1")
    public String userForm() {
        return "user/userForm";
    }

    @PostMapping(path = "/add_v1")
    public String handleUserForm(@RequestParam(name = "name") String name,
                                 @RequestParam(name = "surname") String surname) {
        userService.createUser(name, surname);

        return "redirect:/user/list";
    }

    // zadanie: stwórz nową metodę (nowy mapping) w
    // kontrolerze na adresie /user/list. Metoda powinna
    // ładować widok z listą userList z klasy UserService.
    //
    // Przekaż listę z UserService do widoku (użyj Modelu)
    // Na widoku (w html) wyświetl tabelkę. Tabelka powinna
    // mieć nagłówek oraz kolumnę: Lp., Imie, Nazwisko
    //
    // Liczbę porządkową wyciągnij z iteratora.
    @GetMapping(path = "/list")
    public String displayUserList(Model model) {
        model.addAttribute("userList", userService.getUserList());

        return "user/userList";
    }

    @GetMapping(path = "/add")
    public String addUserForm(Model model) {
        model.addAttribute("createdUser", new User());

        return "user/addUserForm";
    }

    @PostMapping(path = "/add")
    public String handleUserAddForm(User createdUser, Model model) {
        try {
            userService.createUser(createdUser);
        } catch (RequiredFieldEmptyException e) {
            model.addAttribute("createdUser", createdUser);
            model.addAttribute("errorMessage", "Please write Your surname.");

            return "user/addUserForm";
        }

        return "redirect:/user/list";
    }

}
