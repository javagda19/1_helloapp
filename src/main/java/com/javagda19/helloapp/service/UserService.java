package com.javagda19.helloapp.service;

import com.javagda19.helloapp.exception.RequiredFieldEmptyException;
import com.javagda19.helloapp.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    // obsługa danych i przetworzenie ich w  odpowiedni sposób
    //  = przekazywanie i obsługa bazy danych.

    private List<User> userList = new ArrayList<>();

    public void createUser(String name, String surname) {
        User newUser = new User(name, surname);
        userList.add(newUser);
    }

    public List<User> getUserList() {
        return userList;
    }

    public void createUser(User createdUser) throws RequiredFieldEmptyException {
        if (createdUser.getSurname() == null || createdUser.getSurname().isEmpty()) {
            throw new RequiredFieldEmptyException();
        }
        userList.add(createdUser);
    }
}
